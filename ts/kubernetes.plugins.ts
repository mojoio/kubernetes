// node native scope
import * as path from 'path';

export { path };

// tsclass scope
import * as tsclass from '@tsclass/tsclass';

export { tsclass };

// pushrocks scope
import * as smartpath from '@pushrocks/smartpath';
import * as smartstring from '@pushrocks/smartstring';

export { smartpath, smartstring };

// third party scope
import * as kubeclient from 'kubernetes-client';

export { kubeclient as kubectl };
