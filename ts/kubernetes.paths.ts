import * as plugins from './kubernetes.plugins';

export const defaultKubeConfigPath = plugins.path.join(
  plugins.smartpath.get.home(),
  '.kube/config'
);
